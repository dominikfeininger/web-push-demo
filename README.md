# Demo for the Web Push blog

This is the working demo code for the blog post here:

[https://blog.atulr.com/web-notifications](https://blog.atulr.com/web-notifications)

To launch frontend:

```sh
npm install -g http-server (optional)
npm start
```
