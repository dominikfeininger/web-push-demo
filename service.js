// urlB64ToUint8Array is a magic function that will encode the base64 public key
// to Array buffer which is needed by the subscription option
const urlB64ToUint8Array = base64String => {
  const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, "+")
    .replace(/_/g, "/");
  const rawData = atob(base64);
  const outputArray = new Uint8Array(rawData.length);
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
};

const saveSubscription = async subscription => {
  const SERVER_URL = "/subscription/Subscriptions";
  // const SERVER_URL = "http://localhost:4000/save-subscription";

  // parse cap friendy
  const formattedSub = JSON.parse(JSON.stringify(subscription))
  const tmpSubscription = {
    "endpoint": formattedSub.endpoint,
    "expirationTime": null,
    "p256dh": formattedSub.keys.p256dh,
    "auth": formattedSub.keys.auth
  }
  const body = JSON.stringify(tmpSubscription)
  // const body = JSON.stringify(subscription)

  const response = await fetch(SERVER_URL, {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: body
  });
  return response.json();
};

self.addEventListener('activate', async () => {
  // This will be called only once when the service worker is activated.
  try {
    const applicationServerKey = urlB64ToUint8Array(
      'BFSTxIx-s7w3P3lmGcglu6cRloXHb0C4PoWtvOW9dtZYIN7jaaIRmsY_SO2PBxHS_2qvkdiZbATSW11vGpNMw3o'
    )
    const options = { applicationServerKey, userVisibleOnly: true }
    const subscription = await self.registration.pushManager.subscribe(options)
    const response = await saveSubscription(subscription)
    console.log(response)
  } catch (err) {
    console.log('Error', err)
  }
})

const getSubscription = async () => {
  return await self.registration.pushManager.getSubscription()
}

self.addEventListener("push", function (event) {
  if (event.data) {
    console.log("Push event!! ", event.data.text());
    showLocalNotification("Yolo", event.data.text(), self.registration);
  } else {
    console.log("Push event but no data");
  }
});

const showLocalNotification = (title, body, swRegistration) => {
  const options = {
    body
    // here you can add more properties like icon, image, vibrate, etc.
  };
  swRegistration.showNotification(title, options);
};
